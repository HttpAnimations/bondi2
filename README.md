# Bondi2
A whole new remake of Bondi.

![photo](photos/main.png)

# Installing
To install prebuilt libarys head [here](install.md)

# Building
To build Bondi run the following commands.

### Dependencies
#### Wine
```bash
sudo pacman -Syu wine -- Arch
sudo apt-get install wine --Ubuntu/PopOS/Debian
sudo dnf install wine --Fedora/Redhat
```

#### Mono

```bash
sudo pacman -Suy mono -- Arch
sudo apt-get install mono-complete -- Ubuntu/PopOS/Debian
sudo dnf install mono-complete -- Fedora/Redhat
```

## Start building

```bash
git clone -b main https://gitlab.com/HttpAnimations/bondi2.git
cd bondi2
npm run build 
```

If you would like to compile for only system use this command.

```bash
npm run make
```

## Mainly adding/editing the config
To edit the config open **resources/app/games.json**

## Local icons
If you are using icons for **"kintendo"** games they will often get removed from most websites so if you want to use local photos then enter the full path for the photo. 

## Art
Good place to get game art is [https://www.steamgriddb.com/](https://www.steamgriddb.com/).

