# Bondi2/installing
A whole new remake of Bondi. **-1.0.6 are testing version and don't have a download.**

# 1.1.2
## [Linux - RPM](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-1.1.2-1.x86_64.rpm)
## [Linux - DEB](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi_1.1.2_amd64.deb)
## [macOS](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-darwin-x64-1.1.2.zip)
## [NT](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-1.1.2.Setup.exe)

# 1.1.1
## [Linux - RPM](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-1.1.1-1.x86_64.rpm)
## [Linux - DEB](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi_1.1.1_amd64.deb)
## [macOS](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-darwin-x64-1.1.1.zip)
## [NT](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-1.1.1.Setup.exe)


# 1.1.0
## [Linux - RPM](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-1.1.0-1.x86_64.rpm)
## [Linux - DEB](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi_1.1.0_amd64.deb)
## [macOS](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-darwin-x64-1.1.0.zip)
## [NT](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-1.1.0.Setup.exe)

# 1.0.9
## [Linux - RPM](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-1.0.9-1.x86_64.rpm)
## [Linux - DEB](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi_1.0.9_amd64.deb)
## [macOS](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-darwin-x64-1.0.9.zip)
## [NT](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-1.0.9.Setup.exe)

# 1.0.8
## [Linux - RPM](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-1.0.8-1.x86_64.rpm)
## [Linux - DEB](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi_1.0.8_amd64.deb)
## [macOS](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-darwin-x64-1.0.8.zip)
## [NT](https://github.com/HttpAnimation/bondi2/releases/download/2/bondi-1.0.8.Setup.exe)

# 1.0.7
## [Linux - RPM](https://github.com/HttpAnimation/bondi2/releases/download/1/bondi-1.0.7-1.x86_64.rpm)
## [Linux - DEB](https://github.com/HttpAnimation/bondi2/releases/download/1/bondi_1.0.7_amd64.deb)
## [macOS](https://github.com/HttpAnimation/bondi2/releases/download/1/bondi-darwin-x64-1.0.7.zip)
## [NT](https://github.com/HttpAnimation/bondi2/releases/download/1/bondi-1.0.7.Setup.exe)