function fetchVersion() {
    fetch('package.json')
        .then(response => response.json())
        .then(data => {
            const versionDiv = document.getElementById('version');
            versionDiv.innerHTML = '<h3>Version: ' + data.version + '</h3>'; // Wrap version in <h3> tag
        })
        .catch(error => console.error('Error loading the version:', error));
}
