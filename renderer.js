const { ipcRenderer } = require('electron');

document.addEventListener('DOMContentLoaded', () => {
  const homeScreen = document.getElementById('home-screen');
  const addGameScreen = document.getElementById('add-game-screen');
  const gamesList = document.getElementById('games-list');
  const addGameButton = document.getElementById('add-game-button');
  const backButton = document.getElementById('back-button');
  const addGameForm = document.getElementById('add-game-form');
  const addAnotherGameButton = document.getElementById('add-another-game');
  const categoryButtons = document.getElementById('category-buttons');

  let games = [];

  // Function to load games from main process
  const loadGames = () => {
    ipcRenderer.send('get-games');
  };

  // Function to display games on the UI
  const displayGames = (category) => {
    gamesList.innerHTML = '';
    const filteredGames = category ? games.filter(game => game.category === category) : games;

    filteredGames.forEach(game => {
      const gameItem = document.createElement('div');
      gameItem.classList.add('game-item');
      gameItem.innerHTML = `
        <img src="${game.boxArt}" alt="${game.name}">
        <h3>${game.name}</h3> 
      `;
      gameItem.style.backgroundColor = game.categoryColor || '#6a477c';
      gameItem.addEventListener('click', () => {
        require('child_process').exec(game.launchCommand);
      });
      gamesList.appendChild(gameItem);
    });
  };

  // Function to create category buttons
  const createCategoryButtons = () => {
    categoryButtons.innerHTML = '';
    const categories = [...new Set(games.map(game => game.category))];

    categories.forEach(category => {
      const categoryButton = document.createElement('button');
      const game = games.find(game => game.category === category);
      categoryButton.textContent = category;
      categoryButton.classList.add('category-button');
      categoryButton.style.backgroundColor = game.categoryColor || '#9c27b0';
      categoryButton.addEventListener('click', () => {
        displayGames(category);
      });
      categoryButtons.appendChild(categoryButton);
    });
  };

  // Function to add a new game
  const addGame = () => {
    const game = {
      name: document.getElementById('game-name').value,
      launchCommand: document.getElementById('launch-command').value,
      boxArt: document.getElementById('box-art').value,
      category: document.getElementById('category').value,
      categoryColor: document.getElementById('category-color').value,
    };
    ipcRenderer.send('add-game', game);
  };

  // Event listeners for UI buttons

  addGameButton.addEventListener('click', () => {
    homeScreen.style.display = 'none';
    addGameScreen.style.display = 'block';
  });

  backButton.addEventListener('click', () => {
    homeScreen.style.display = 'block';
    addGameScreen.style.display = 'none';
  });

  addGameForm.addEventListener('submit', (event) => {
    event.preventDefault();
    addGame();
    homeScreen.style.display = 'block';
    addGameScreen.style.display = 'none';
  });

  addAnotherGameButton.addEventListener('click', () => {
    addGame();
    addGameForm.reset();
  });

  // Event listener for receiving games data from main process
  ipcRenderer.on('games-data', (event, receivedGames) => {
    games = receivedGames;
    createCategoryButtons();
    displayGames();
  });

  // Initial load of games when the renderer process starts
  loadGames();
});
