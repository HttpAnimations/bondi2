# Bondi2/Runners/Steam
Runners for steam.

[Freddy Fazbear's Pizzeria Simulator](https://store.steampowered.com/app/738060/Freddy_Fazbears_Pizzeria_Simulator/)

```bash
steam steam://rungameid/738060
```

[Fireworks Mania - An Explosive Simulator](https://store.steampowered.com/app/1079260/Fireworks_Mania__An_Explosive_Simulator/)
```bash
steam steam://rungameid/1079260
```

[Five Nights at Freddy's: Sister Location](https://store.steampowered.com/app/506610/Five_Nights_at_Freddys_Sister_Location/)
```bash
steam steam://rungameid/506610
```

[Fallout Shelter](https://store.steampowered.com/app/588430/Fallout_Shelter/)
```bash
steam steam://rungameid/588430
```

[Beamng.drive](https://store.steampowered.com/app/284160/BeamNGdrive/)
```bash
steam steam://rungameid/284160
```

[Furry Shades of Gay](https://store.steampowered.com/app/1399930)
```bash
steam steam://rungameid/1399930
```

[Teardown](https://store.steampowered.com/app/1167630/Teardown/)
```bash
steam steam://rungameid/1167630
```

[Everyone Dies](https://store.steampowered.com/app/1203560/Everyone_Dies/)
```bash
steam steam://rungameid/1203560
```