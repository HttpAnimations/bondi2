# Bondi2/Runners/Flatpak
Runners for Flatpak.

[Plex](https://flathub.org/apps/tv.plex.PlexDesktop)

```bash
flatpak run tv.plex.PlexDesktop
``` 

[Plex HTPC](https://flathub.org/apps/tv.plex.PlexHTPC)

```bash
flatpak install flathub tv.plex.PlexHTPC
```

[Plex AMP](https://flathub.org/apps/com.plexamp.Plexamp)

```bash
flatpak install flathub com.plexamp.Plexamp
```


[Girens for Plex](https://flathub.org/apps/nl.g4d.Girens)

```bash
flatpak install flathub nl.g4d.Girens
```
