clear

echo "Compiling for Linux zip/deb/rpm."
npm run make -- --platform linux

echo "Compiling for NT exe."
npm run make -- --platform win32

echo "Compling for Darwin zip->app."
npm run make -- --platform darwin

echo "Done packages can be found Root:/out/make/"
