const { app, BrowserWindow, ipcMain } = require('electron');
const fs = require('fs');
const path = require('path');

let mainWindow;

const createWindow = () => {
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  mainWindow.loadFile('index.html');
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

ipcMain.on('get-games', (event) => {
  const games = JSON.parse(fs.readFileSync(path.join(__dirname, 'games.json')));
  event.sender.send('games-data', games);
});

ipcMain.on('add-game', (event, newGame) => {
  const gamesPath = path.join(__dirname, 'games.json');
  let games = JSON.parse(fs.readFileSync(gamesPath));

  // Check if the game already exists
  const existingGameIndex = games.findIndex(game => game.name === newGame.name);
  if (existingGameIndex !== -1) {
    // Game already exists, you can update it or skip adding
    // For simplicity, we'll update the existing game with new data
    games[existingGameIndex] = newGame;
  } else {
    // Game does not exist, add it to the array
    games.push(newGame);
  }

  // Write back to the JSON file
  fs.writeFileSync(gamesPath, JSON.stringify(games, null, 2));

  // Send updated games list to renderer process
  event.sender.send('games-data', games);
});